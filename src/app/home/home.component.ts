import { Component, OnInit } from '@angular/core';
import {EmployeeExampleService} from "../servicios/employee-example.service";
import 'rxjs/add/operator/map'; // Realizar el mapeo a json
import { NgForm } from '@angular/forms';
/**Este debe ser el modelo que el metodo post del rest espera */
class employee {
  ID: number;
  fullName: string;
  typeContract2: string;
  rate: string;
  dateCreation: string;
  seleccionado: boolean;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  
  // ListaModelo: any;
  ListaModelo: employee [] = [];  /**Cambio a una lista de este tipo de objeto */
  Modelo: employee ; /**Instancia de la clase creada previamente, necesaria para el put y post */
  banNuevo: boolean;

  constructor(private employeeServicio: EmployeeExampleService) {  
    this.Modelo = new employee();
    // this.Modelo.dateCreation = null;
    this.banNuevo=false;
  }

  /**Es el que inicial el angular en componente asoscioa */
  ngOnInit() {
    this.cargarEmployees();
  }

  cargarEmployees(){
      this.employeeServicio.getAll().subscribe(result => {
        this.ListaModelo = result;
        // console.log(this.ListaModelo);
      },
      error => {
        alert("Se presenta un error en la conexion");
      });
  }

  crearEmployee(){
    this.employeeServicio.post(this.Modelo).subscribe(result => {
      if (result === "Record added!"){

        this.ListaModelo.push(this.Modelo);/**De esta forma gregamos el registro a la variable iterada y auto se pinta */
        this.Modelo = new employee(); /**Instanciamos como si fuera desde cero mi modelo */
        this.banNuevo = false;
        alert("Registro de forma exitosa");
      }
    },
    error => {
      alert("Se presenta un error en la conexion");
    });
  }

  eliminarEmploye(idEmployee: number){
    debugger
    this.employeeServicio.delete(idEmployee).subscribe(result => {
      if (result === "Record removed!"){

        // let r= this.ListaModelo.find();
        alert("Eliminacion exitosa del ID: " + idEmployee);
      }
    },
    error => {
      alert("Se presenta un error en la conexion");
    });
  }

  actualizarEmploye(e){
    alert("metodo no implementado");
  }

  eliminacionMasivaEmployeesChecked(){
      this.ListaModelo.forEach(element => {
        debugger
        //**ACCION UY ACCION A CADA ELEMENTO CHEQUECHADO /**Para eliminacion masiva */
        if(element.seleccionado){
            this.eliminarEmploye(element.ID);
        }
      });
  }
}
