import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { RouterModule , Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HomeComponent } from './home/home.component';
import { EmployeeExampleService } from './servicios/employee-example.service';
import { FormsModule } from '@angular/forms';
// import { HttpClientModule, HttpClient} from '@angular/common/http'; 

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, /**StaticInjectorError(AppModule)[EmployeeExampleService -> Http]: */
  ],
  providers: [
    EmployeeExampleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
