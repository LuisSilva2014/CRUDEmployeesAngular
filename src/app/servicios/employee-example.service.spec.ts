import { TestBed, inject } from '@angular/core/testing';

import { EmployeeExampleService } from './employee-example.service';

describe('EmployeeExampleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeExampleService]
    });
  });

  it('should be created', inject([EmployeeExampleService], (service: EmployeeExampleService) => {
    expect(service).toBeTruthy();
  }));
});
