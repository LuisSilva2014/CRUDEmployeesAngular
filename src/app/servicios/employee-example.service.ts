import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map'; // Realizar el mapeo a json

@Injectable()
export class EmployeeExampleService {

  /**Estos variables */
  Model: any = {};
  headerss = new Headers({ 'Content-Type': 'application/json' });
  myParams = new URLSearchParams();
  UrlApi = 'http://luis-silva-temp2.silva-solutions.com/api/employee/';
  Ejemple = '';

  constructor(private http: Http) { 
    this.Ejemple ="Mi valor por inicial";
  }

  /**Metodo para consultar todos los empleados */
  getAll() {
    return this.http.get(this.UrlApi).map(result => {
      return result.json();
   });
  }

   /**Metodo para guardar */
   post(Model) {
    return this.http.post(this.UrlApi, JSON.stringify(Model), { headers: this.headerss }).map(result => {
        return result.json();
    });
  }

  /**Metodo para eliminar */
  delete(id: number) {
      return this.http.delete(this.UrlApi + '?id=' + id, { headers: this.headerss }).map(result => {
        return result.json();
      });
  }

  /**Metodo para actualizar un empleado */
  put(NameObj: string) {
      return this.http.delete(this.UrlApi + '?Name=' + NameObj, { headers: this.headerss }).map(result => {
        return result.json();
      });
  }

}